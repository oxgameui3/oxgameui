package nut.oxgameui;

import java.io.Serializable;


public class Player implements  Serializable {

    private char name;
    private int win, lose, draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void Win() {
        win++;
    }

    public int getLose() {
        return lose;
    }

    public void Lose() {
        lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void Draw() {
        draw++;
    }

    public Player(char name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
